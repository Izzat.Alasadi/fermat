import random
import math

def get_all_prime(n):
    prime_list = []
    for i in range(1, n):
        if i == 0 or i == 1:
            continue
        else:
            for j in range(2, int(i/2)+1):
                if i % j == 0:
                    break
            else:
                prime_list.append(i)
    return prime_list

def get_all_coprime(n):
    all_coprime=[]
    for i in range(1,n):
        if math.gcd(i, n) == 1:
            all_coprime.append(i)
    return all_coprime

def get_coprime(n):
    while True:
        coprime = random.randrange(n)
        if math.gcd(coprime, n) == 1:
            return coprime
 
 
def FermatPrimalityTest(number):
    if (number > 1):
        #repeat 3 times
        for _ in range(3):
            randomNumber = random.randint(2, number)-1
            if ( pow(randomNumber, number-1, number) != 1 ):
                return False
        return True
    else: 
        return False  

def cipher_numbers(n):
    a = int( math.ceil( n**0.5 ) )
    b2 = a * a - n
    b = int( b2 ** 0.5 ) 

    count = 0
    while b * b != b2:
        a = a + 1
        b2 = a * a - n
        b = int( b2 ** 0.5 )
        count += 1
    p = a + b
    q= a - b
    n= p * q
    m= (p - 1) * (q - 1)
    
    print()
    print('a= ', a)
    print('b= ', b)
    print('p= ', p)
    print('q= ', q)
    print('n= p*q =', n)
    print('m= (p-1)(q-1)= ', m)
    
    return p, q


def main():
    d=int(input("Enter d= "))

    if not FermatPrimalityTest(d): print("{} is not prime number".format(d))
    
    print("primes in range 1 - ",d,end="\n")
    for e in range(d):
        if FermatPrimalityTest(e): 
            print("{}".format(e), end=", ")
            #if math.gcd(e,d)==1:
    print("")
    sqd=math.ceil(math.sqrt(d))
    print("primes in range 1 -sqr({}) ".format(sqd),end="\n")
    for e in range(sqd):
        if FermatPrimalityTest(e): 
            print("{}".format(e), end=", ")
    p,q=cipher_numbers(d)

    print("")
    print("primes P in range 1 - {} ".format(p),end="\n")
    for i in range (p):
        if FermatPrimalityTest(i): 
            print("{}".format(i), end=", ")

    print("")
    print("primes Q in range 1 - {} ".format(q),end="\n")
    for i in range (q):
        if FermatPrimalityTest(i): 
            print("{}".format(i), end=", ")

  
if __name__=="__main__":
    main()

